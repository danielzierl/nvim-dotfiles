local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
-- vim.keymap.set('n', '<leader>e', ':Ex<CR>', {})

vim.api.nvim_set_keymap('n', '<leader>gp', ':ChatGPT<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<leader>st', ':Speedtyper<CR>', { noremap = true })
vim.keymap.set('n', 'no', 'o<Esc>k', {})
vim.keymap.set('n', 'nO', 'O<Esc>j', {})
vim.keymap.set('n', 'vs', 'vi\'', { noremap = true })
vim.keymap.set('n', 'vd', 'vi\"', { noremap = true })
vim.cmd([[command! -nargs=0 W w]])


-- Vertical split with Ctrl+v
vim.api.nvim_set_keymap('n', '<C-q>', ':vsplit<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-b>', ':split<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<A-f>', ':call ToggleMaximizeCurrentWindow()<CR>', { noremap = true })
-- Switch between splits using Ctrl+h and Ctrl+l
vim.api.nvim_set_keymap('n', '<C-h>', '<C-w>h', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-l>', '<C-w>l', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-j>', '<C-w>j', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-k>', '<C-w>k', { noremap = true })


vim.api.nvim_set_keymap('n', '<Left>', ":echo 'use h'<CR>", { noremap = true })
vim.api.nvim_set_keymap('n', '<Right>', ":echo 'use l'<CR>", { noremap = true })
vim.api.nvim_set_keymap('n', '<Up>', ":echo 'use k'<CR>", { noremap = true })
vim.api.nvim_set_keymap('n', '<Down>', ":echo 'use j'<CR>", { noremap = true })

vim.api.nvim_set_keymap('i', '<Left>', "", { noremap = true })
vim.api.nvim_set_keymap('i', '<Right>', "", { noremap = true })
vim.api.nvim_set_keymap('i', '<Up>', "", { noremap = true })
vim.api.nvim_set_keymap('i', '<Down>', "", { noremap = true })

vim.api.nvim_set_keymap('', 'Z', 'J', { noremap = true, silent = true })
-- Move cursor up faster
vim.api.nvim_set_keymap('', 'K', '7k', { noremap = true, silent = true })

-- Move cursor down faster
vim.api.nvim_set_keymap('', 'J', "7j", { noremap = true, silent = true })

-- Move selected lines up
vim.api.nvim_set_keymap('v', '<M-Up>', ":m '<-2<CR>gv=gv", { noremap = true })
-- Move selected lines down
vim.api.nvim_set_keymap('v', '<M-Down>', ":m '>+1<CR>gv=gv", { noremap = true })

vim.api.nvim_set_keymap("n", "<leader>z", "za", { noremap = true })
-- zen mode
vim.api.nvim_set_keymap("n", "``", ":ZenMode<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<leader>1", ":ClangdSwitchSourceHeader<CR>", { noremap = true })

-- telescope
function vim.getVisualSelection()
    vim.cmd('noau normal! "vy"')
    local text = vim.fn.getreg('v')
    vim.fn.setreg('v', {})

    text = string.gsub(text, "\n", "")
    if #text > 0 then
        return text
    else
        return ''
    end
end

local keymap = vim.keymap.set
local tb = require('telescope.builtin')
local opts = { noremap = true, silent = true }

keymap('n', '<space>f', ':Telescope current_buffer_fuzzy_find<cr>', opts)
keymap('v', '<space>f', function()
    local text = vim.getVisualSelection()
    tb.current_buffer_fuzzy_find({ default_text = text })
end, opts)

keymap('n', '<space>g', ':Telescope live_grep<cr>', opts)
keymap('v', '<space>g', function()
    local text = vim.getVisualSelection()
    tb.live_grep({ default_text = text })
end, opts)
