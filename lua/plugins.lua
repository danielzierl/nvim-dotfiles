-- This file can be loaded by calling `lua require('plugins')` from your init.vim
-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]



return require('packer').startup(function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'
    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.2',
        -- or                            , branch = '0.1.x',
        requires = {
            'nvim-lua/plenary.nvim',
            "debugloop/telescope-undo.nvim",
            "nvim-telescope/telescope-live-grep-args.nvim",
        }
    } use { "ellisonleao/gruvbox.nvim" }
  use { 'sainnhe/sonokai' }
  use {
    "loctvl842/monokai-pro.nvim",
    config = function()
      require("monokai-pro").setup()
    end
  }
  use 'sainnhe/everforest'
  use({ 'rose-pine/neovim', as = 'rose-pine' })
  use 'catppuccin/nvim'
  use 'luisiacc/gruvbox-baby'
  use 'nvim-treesitter/nvim-treesitter-context'
  -- use 'Exafunction/codeium.vim'
  use({
    "Exafunction/codeium.vim",
    commit = "a1c3d6b369a18514d656dac149de807becacbdf7",
  })
  use 'AlexvZyl/nordic.nvim'
  use 'ayu-theme/ayu-vim'
  use 'pmalek/toogle-maximize.vim'
  use 'joshdick/onedark.vim'
  use 'lambdalisue/suda.vim'
  -- use 'kamykn/spelunker.vim'
  use({
    "kdheepak/lazygit.nvim",
    -- optional for floating window border decoration
    requires = {
      "nvim-lua/plenary.nvim",
    },
  })

  use('nvim-treesitter/nvim-treesitter', { run = ':TSUpdate' })
  use 'williamboman/mason.nvim'
  use {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v3.x',
    requires = {
      { 'williamboman/mason.nvim' },
      { 'williamboman/mason-lspconfig.nvim' },

      -- LSP Support
      { 'neovim/nvim-lspconfig' },
      -- Autocompletion
      { 'hrsh7th/nvim-cmp' },
      { 'hrsh7th/cmp-nvim-lsp' },
      { 'L3MON4D3/LuaSnip' },
    }
  } -- use 'tpope/vim-commentary'
  use {
    "windwp/nvim-autopairs",
    config = function() require("nvim-autopairs").setup {} end
  }
  use 'sbdchd/neoformat'
  use { 'nvim-lualine/lualine.nvim' }

  -- use {
  --   'MarcHamamji/runner.nvim',
  --   requires = {
  --     'nvim-telescope/telescope.nvim',
  --     requires = { 'nvim-lua/plenary.nvim' }
  --   },
  --   config = function() require('runner').setup() end
  -- }
  use "kyazdani42/nvim-web-devicons"
  use {
    "nvim-tree/nvim-tree.lua",
    after = "nvim-web-devicons",
    requires = "nvim-tree/nvim-web-devicons"
  }
  use 'lewis6991/gitsigns.nvim' -- OPTIONAL: for git status
  -- use 'romgrk/barbar.nvim'
  use {
    'filipdutescu/renamer.nvim',
    branch = 'master',
    requires = { { 'nvim-lua/plenary.nvim' } }
  }
  use 'RRethy/vim-illuminate'
  use 'ggandor/leap.nvim'
  use 'mg979/vim-visual-multi'
  use {
    'numToStr/Comment.nvim',
    config = function() require('Comment').setup() end
  }
  -- use {"airblade/vim-gitgutter"}
  -- use 'vim-airline/vim-airline'
  use {
    "akinsho/toggleterm.nvim",
    tag = '*',
    config = function() require("toggleterm").setup() end
  }
  use { "f-person/git-blame.nvim" }
  use({
    "ziontee113/color-picker.nvim",
    config = function()
      require("color-picker")
    end,
  })
  use { "brenoprata10/nvim-highlight-colors" }
  -- use({
  --   "jackMort/ChatGPT.nvim",
  --     config = function()
  --       require("chatgpt").setup()
  --     end,
  --     requires = {
  --       "MunifTanjim/nui.nvim",
  --       "nvim-lua/plenary.nvim",
  --       "nvim-telescope/telescope.nvim"
  --     }
  -- })
  use {
    "NStefan002/speedtyper.nvim",
    branch = "main",
    config = function()
      require('speedtyper').setup({
        -- your config
      })
    end }
  use "folke/zen-mode.nvim"
  use "folke/persistence.nvim"

  use "folke/twilight.nvim"
  use({ "navarasu/onedark.nvim"})
  use "kyazdani42/nvim-web-devicons"
  use {
	  "nvim-tree/nvim-tree.lua",
	  after = "nvim-web-devicons",
	  requires = "nvim-tree/nvim-web-devicons"
  }
  use 'lewis6991/gitsigns.nvim' -- OPTIONAL: for git status
  -- use 'romgrk/barbar.nvim'
  use {
	  'filipdutescu/renamer.nvim',
	  branch = 'master',
	  requires = { { 'nvim-lua/plenary.nvim' } }
  }
  use 'RRethy/vim-illuminate'
  use 'ggandor/leap.nvim'
  use 'mg979/vim-visual-multi'
  use {
	  'numToStr/Comment.nvim',
	  config = function() require('Comment').setup() end
  }
  -- use {"airblade/vim-gitgutter"}
  -- use 'vim-airline/vim-airline'
  use {
	  "akinsho/toggleterm.nvim",
	  tag = '*',
	  config = function() require("toggleterm").setup() end
  }
  use { "f-person/git-blame.nvim" }
  use({
	  "ziontee113/color-picker.nvim",
	  config = function()
		  require("color-picker")
	  end,
  })
  use { "brenoprata10/nvim-highlight-colors" }
  use 'akinsho/flutter-tools.nvim'

    use {
        "rcarriga/nvim-notify",
        config = function()
            require("notify").setup {
                background_colour = "#000000",
                render = "compact",
                timeout = 3000,

            }
        end
    }
    use "folke/tokyonight.nvim"
end)
