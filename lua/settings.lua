vim.g.mapleader = " "
local highlight = vim.highlight
local g = vim.g
local o = vim.o

o.encoding = "utf-8"

-- Set tabsize
o.tabstop = 2
o.shiftwidth = 2
o.softtabstop = 0
o.expandtab = true
o.numberwidth = 1
-- Basic settings
o.termguicolors = true
o.relativenumber = true
o.number = true
o.cursorline = true
o.wrap = true
o.expandtab = true
o.autochdir = false
o.showcmd = true
o.showmatch = true
o.showmode = true
o.hidden = true
o.scrolloff = 5
o.autoindent = true
o.autoread = true
o.wildmenu = true
o.wildmode = "list:longest"
o.wildignore = "*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*xlsx"
o.backspace = "indent,eol,start"
-- Search
o.hlsearch = false
o.incsearch = true

g.rehash256 = 1

-- Enable mouse
o.mouse = "a"

-- Clipboard
o.clipboard = "unnamedplus"

-- Decrease update time
o.timeoutlen = 300
o.updatetime = 50

-- Undofile
o.undofile = true
o.undolevels = 1000
o.undoreload = 10000
o.showcmd = false   
--Folds
-- o.foldmethod = "indent"
-- o.foldlevel = 0

vim.cmd.colorscheme "gruvbox"
vim.cmd([[au BufEnter * set noro]])

vim.cmd([[ hi Normal guibg=NONE ctermbg=NONE ]])     -- Make background transparent
vim.cmd([[ hi SignColumn guibg=NONE ctermbg=NONE ]]) -- Make sign column transparent
vim.cmd([[ hi VertSplit guibg=NONE ctermbg=NONE ]])  -- Make vertical split separator transparent
-- vim.api.nvim_win_set_option(0, 'winblend',90)
--
-- Open all folds by default
vim.cmd('autocmd BufWinEnter * normal! zR')

vim.g.neoformat_enabled_javascript = { 'prettier' }
vim.g.neoformat_enabled_typescript = { 'prettier' }
vim.g.neoformat_enabled_html = { 'prettier' }
vim.g.neoformat_enabled_lua = { 'luaformat' }
vim.g.neoformat_enabled_python = { 'autopep8', 'prettier' }
vim.g.neoformat_enabled_svelte = { 'prettier' }


-- vim.cmd('autocmd BufWritePre * lua vim.lsp.buf.format()')
vim.cmd('autocmd BufWritePre *.ts Neoformat')
vim.cmd('autocmd BufWritePre *.html Neoformat')
vim.cmd('autocmd BufWritePre *.lua Neoformat')
vim.cmd('autocmd BufWritePre *.py Neoformat')
vim.cmd('autocmd BufWritePre *.svelte Neoformat')
vim.cmd('autocmd BufWritePre *.cpp Neoformat')
vim.cmd('autocmd BufWritePre *.h Neoformat')
vim.cmd('autocmd BufWritePre *.c Neoformat')
-- vim
--  " trigger `autoread` when files changes on disk
--
--
function check_time_if_not_insert()
  -- print("check_time_if_not_insert")
  if vim.api.nvim_get_mode() ~= "i" then
    pcall(vim.api.nvim_command, ":checktime")
  end
end

vim.api.nvim_create_autocmd(
  { "FocusGained", "BufEnter", "CursorHold" },
  {
    callback = check_time_if_not_insert,
  })
--
