require('telescope').setup {
    defaults = {
        initial_mode = "normal",
        sorting_strategy = 'ascending',
        layout_strategy = 'horizontal',
        layout_config = {
            horizontal = {
                preview_width = 0.6,
                results_width = 0.8,
                height = 0.95,
                width = 0.95,
            },
        },
        mappings = {
            i = {
                ['<c-d>'] = require('telescope.actions').delete_buffer
            },
            n = {
                ['<c-d>'] = require('telescope.actions').delete_buffer
            }

        },
        file_previewer = require('telescope.previewers').vim_buffer_cat.new,
        -- sorting_strategy = 'descending',
        path_display = { 'absolute' },
        highlight = "TelescopePreviewNormal",
        file_ignore_patterns = {},
        file_sorter = require('telescope.sorters').get_fuzzy_file,
        generic_sorter = require('telescope.sorters').get_generic_fuzzy_sorter,

    },
    extensions = {
        undo = {

        }
    }
}

require('telescope').load_extension('undo')
