-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- set termguicolors to enable highlight groups

-- vim.keymap.set('n', '<C-e>',":NvimTreeToggle<CR>",{})

local api = require "nvim-tree.api"
local zenmode= require "zen-mode"
local zenmode_view = require "zen-mode.view"

local function opts(desc)
    return {
        desc = "nvim-tree: " .. desc,
        buffer = bufnr,
        noremap = true,
        silent = true,
        nowait = true
    }
end
local view = require('nvim-tree.view')
-- local function toggleFocus()
--   if view.is_visible() then
--     api.tree.close()
--   else
--     api.tree.focus()
--   end
-- end
-- default mappings
-- api.config.mappings.default_on_attach(bufnr)
local function toggle()
    local curr_open = zenmode_view.is_open()
    local tree_visible_before = api.tree.is_visible()
    if curr_open and not tree_visible_before then
        zenmode.close()
    end

    vim.cmd[[NvimTreeToggle]]

end


-- custom mappings
vim.keymap.set('n', '<leader>e', toggle)
-- vim.keymap.set('n', '<leader>f', ":NvimTreeFocus<CR>")

-- pass to setup along with your other options
require("nvim-tree").setup {
    update_focused_file = {enable = true},
    on_attach = my_on_attach,
    actions = {open_file = {quit_on_open = true}},
    view = {
      width=50,
    },
    filters = {
        dotfiles = false,
        git_ignored = false,
        enable = true
    } 
    -- filesystem = {
    --     filtered_items = {
    --         hide_dotfiles = false,
    --         visible = true,
    --         hide_gitignored = false
    --     }
    -- }

}

