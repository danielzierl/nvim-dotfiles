local lsp = require('lsp-zero').preset({})

lsp.on_attach(function(client, bufnr)
  lsp.default_keymaps({ buffer = bufnr })
  vim.keymap.set('n', '<leader>di', vim.diagnostic.open_float)
  vim.keymap.set('n', '<leader>dl', vim.diagnostic.setloclist)
  vim.keymap.set('n', '<leader>h', vim.lsp.buf.hover)
  vim.keymap.set('n', '<leader>s', vim.lsp.buf.signature_help)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action)
  vim.diagnostic.config({
    virtual_text = false
  })
  -- vim.o.updatetime = 500
  -- vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]
end)

local root_pattern = require('lspconfig').util.root_pattern('.git')

require('lspconfig').clangd.setup({
  cmd = { "clangd", "--background-index", "--suggest-missing-includes", "--header-insertion=iwyu" },
  root_dir = root_pattern
})
require('lspconfig').gopls.setup({
    settings = {
        gopls = {
            completeUnimported = true,
            usePlaceholders = true,
            analyses = {
                unusedparams = true,
            }
        }
    }
})

vim.api.nvim_create_user_command('CloseFloatingWindows', function(opts)
  for _, window_id in ipairs(vim.api.nvim_list_wins()) do
    -- If window is floating
    local status, win_conf = pcall(vim.api.nvim_win_get_config, window_id)
    if status and win_conf.zindex and tonumber(win_conf.zindex) > 40 then
      -- Force close if called with !
      pcall(vim.api.nvim_win_close, window_id, opts.bang)
    end
  end
end, { bang = true, nargs = 0 })

vim.keymap.set('n', '<leader>q', ':CloseFloatingWindows<CR>')

require('lspconfig').pylsp.setup {
  settings = {
    pylsp = {
      plugins = {
        pycodestyle = {
          ignore = { 'E501' },
          -- maxLineLength = 120,
        }
      }
    }
  }
}


-- Configure flutter
require('flutter-tools').setup({
  lsp = {
    capabilitites = lsp.get_capabilities(),
  }
})


lsp.setup()

-- You need to setup `cmp` after lsp-zero

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()

cmp.setup({
  mapping = {
    -- `Enter` key to confirm completion
    ['<C-y>'] = cmp.mapping.confirm({ select = false }),

    -- Ctrl+Space to trigger completion menu
    ['<C-Space>'] = cmp.mapping.complete(),
    -- Navigate between snippet placeholder
    -- ['<C-w>'] = cmp_action.luasnip_jump_forward(),
    -- ['<C-q>'] = cmp_action.luasnip_jump_backward(),
    ['<C-j>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
    ['<C-k>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
  }
})
