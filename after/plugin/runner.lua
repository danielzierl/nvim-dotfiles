-- require('runner').run()

-- Can also be called with the buffer number where the code is:
-- require('runner').run( <buffer_number> )

-- local e= function ()
--   vim.cmd(':wa')
--   require('runner').run();
-- end
--
-- -- To set a mapping
--
-- vim.keymap.set('n', '<leader><space>', e)
